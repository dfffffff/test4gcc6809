/*
 *
 * Bug testcase for GCC6809
 *
 *  https://gcc.gnu.org/ml/gcc-patches/2008-05/msg01431.html
 *
 *  PASS: -O1 -O2 -O3 -Os
 *  FAIL: -O0
 *
 */

#include <testcase.h>

char var;

//////////////////////////////////////////////////
//                                              //
// function under test                          //
//                                              //
NOINLINE void __attribute__((naked)) dobug(char c __attribute__((unused)))
{
	asm (
		"rts"
	);
}
//                                              //
//////////////////////////////////////////////////

NOINLINE void __attribute__((naked)) check(void)
{
	asm (
		"pshs	u"			"\n\t"
		"ldu	#_var"		"\n\t"
		"ldb	#0xAA"		"\n\t"
		"jsr	_dobug"		"\n\t"
		"puls	u,pc"
	);
}

int main(void)
{
	check();
	return var ? FAIL : PASS;
}
