/*
 *
 * Bug testcase for GCC6809
 *
 *  Fail to compile (ICE) and
 *  check for proper code generation.
 *
 *  OPTIM="-O1 -O2 -O3 -Os"
 *
 */

#include <testcase.h>

NOINLINE int dobug_and_post(int *p)
{
	int i, j;
	j = -1;
	for (i=0; i<32; i++)
		j &= *p++;
	return j;
}

NOINLINE int dobug_and_pre(int *p)
{
	int i, j;
	j = -1;
	for (i=0; i<32; i++)
		j &= *--p;
	return j;
}

NOINLINE int dobug_ior_post(int *p)
{
	int i, j;
	j = 0;
	for (i=0; i<32; i++)
		j |= *p++;
	return j;
}

NOINLINE int dobug_ior_pre(int *p)
{
	int i, j;
	j = 0;
	for (i=0; i<32; i++)
		j |= *--p;
	return j;
}

NOINLINE int dobug_xor_post(int *p)
{
	int i, j;
	j = 0;
	for (i=0; i<32; i++)
		j ^= *p++;
	return j;
}

NOINLINE int dobug_xor_pre(int *p)
{
	int i, j;
	j = 0;
	for (i=0; i<32; i++)
		j ^= *--p;
	return j;
}

NOINLINE uint8 check(uint8 *code, uint8 opcode, uint8 form, uint8 offset)
{
	int i;
	for (i=0; code[i]!=0x39 && code[i]!=0x35; i++) { /* RTS or PULS */
		if (code[i] == opcode)
			break;
	}
#if VERBOSE
	print("%2 ", i);
#endif
	if (code[i+0] == opcode &&			/* OPa indexed */
		(code[i+1]&0x9F) == form &&		/* ,--R or ,R++ */
		code[i+2] == opcode+0x40 &&		/* OPb indexed */
		(code[i+3]&0x9F) == offset)		/* n,R 5-bit */
		return 1;
	return 0;
}

int main(void)
{
	if (!check((uint8*)dobug_and_pre, 0xA4, 0x83, 0x01))
		return FAIL;
	if (!check((uint8*)dobug_xor_pre, 0xA8, 0x83, 0x01))
		return FAIL;
	if (!check((uint8*)dobug_ior_pre, 0xAA, 0x83, 0x01))
		return FAIL;
	if (!check((uint8*)dobug_and_post, 0xA4, 0x81, 0x1F))
		return FAIL;
	if (!check((uint8*)dobug_xor_post, 0xA8, 0x81, 0x1F))
		return FAIL;
	if (!check((uint8*)dobug_ior_post, 0xAA, 0x81, 0x1F))
		return FAIL;
	return PASS;
}
