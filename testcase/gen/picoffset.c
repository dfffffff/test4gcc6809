/*
 *
 * Bug testcase for GCC6809
 *
 *  Check if the generated code use relative addressing mode
 *  when -fpic option is used.
 *
 *  OPTIM="-O1 -O2 -O3 -Os"
 *  CFLAGS="-fpic"
 *
 */

#include <testcase.h>

uint16 val1[2], val2;

void dobug(void)
{
	val1[1] = val2;
}

int main(void)
{
	uint8 *code = (uint8*)dobug;
	if (
		!(code[0] == 0xEC &&		/* ldd mem,pcr*/
		(code[1]&0x9F) == 0x8D &&
		code[4] == 0xED &&			/* std mem,pcr*/
		(code[5]&0x9F) == 0x8D)
		&&
		!(code[0] == 0xAE &&		/* ldx mem,pcr*/
		(code[1]&0x9F) == 0x8D &&
		code[4] == 0xAF &&			/* stx mem,pcr*/
		(code[5]&0x9F) == 0x8D)
		)
		return FAIL;
	return PASS;
}
