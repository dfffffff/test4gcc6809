/*
 *
 * Bug testcase for GCC6809
 *
 *  https://gcc.gnu.org/viewcvs/gcc?view=revision&revision=205117
 *
 *  PASS: -O0 -O2 -O3 -Os
 *  FAIL: -O1
 *
 */

#include <testcase.h>

NOINLINE void check(uint16 value)
{
	static uint16 count;
#if VERBOSE
	print("%2 ", value);
#endif
	if (++count == 16)
		exit(value == 0x11 ? PASS : FAIL);
}

NOINLINE uint8 data(void)
{
	static uint16 seed=0;
	return seed++ ^ 111;
}

//////////////////////////////////////////////////
//                                              //
// function under test                          //
//                                              //
NOINLINE void dobug(void)
{
	uint8 k, s;
	uint16 r;
	for (k = 0; k < 16; k++)
	{
		s = data();
		r = s >> 4;
		if (s)
		{
			if (r)
			{
				if ((k + r) > 256)
					break;
				while (r)
				{
					check(k++);
					r--;
				}
			}
		}
	}
}
//                                              //
//////////////////////////////////////////////////

int main(void)
{
	dobug();
	return FAIL;
}
