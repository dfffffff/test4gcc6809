/*
 *
 * Bug testcase for GCC6809
 *
 *  PASS: none
 *  FAIL: -O0 -O1 -O2 -O3 -Os
 *
 */

#include <testcase.h>

//////////////////////////////////////////////////
//                                              //
// function under test                          //
//                                              //
NORETURN NOINLINE void dobug(int param1, int param2)
{
	if (param1 == 0x1111 && param2 == 0x2222)
		exit(PASS);
	exit(FAIL);
}
//                                              //
//////////////////////////////////////////////////

int main(void)
{
	dobug(0x1111, 0x2222);
	return FAIL;
}

