/*
 *
 * Bug testcase for GCC6809
 *
 *  PASS: -O0
 *  FAIL: -O1 -O2 -O3 -Os
 *
 */

#include <testcase.h>

//////////////////////////////////////////////////
//                                              //
// function under test                          //
//                                              //
NOINLINE char *dobug(char *a, char *b)
{
	while ((*b++ = *a++));
	return --b;
}
//////////////////////////////////////////////////

__attribute__((section(".vector")))	char str[16];

int main(void)
{
	*str = 0;
	dobug("\0X", str);
	return str[1] == 0 ? PASS : FAIL;
}
