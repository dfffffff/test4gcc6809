/*
 *
 * Bug testcase for GCC6809
 *
 *  PASS: -O0 -O1 -O2 -O3
 *  FAIL: -Os
 *
 */

#include <testcase.h>

typedef int (*indcall)(int, int, int, int);

NOINLINE int func(int param1, int param2, int param3, int param4)
{
	(void)param1; (void)param2; (void)param3; (void)param4;
	return 0x5555;
}

NOINLINE int wrong_func(void)
{
	return 0xAAAA;
}

NOINLINE void somefunc(void* param1, int param2, int param3)
{
	if (param3)
		*(char*)param1 = param2;
}

char buf[6];
int param2 = (int)&wrong_func;
int param4 = 0x4444;
indcall icall = func;

//////////////////////////////////////////////////
//                                              //
// function under test                          //
//                                              //
NOINLINE int dobug(void)
{
	int var, ret = -1;
	(*icall)(1, param2, 3, param4);
	for (var=0; var<30; var++) {
		if (icall) {
			ret = (*icall)(1, param2, 3, param4);
			if (ret)
				break;
		}
	}
	somefunc(buf, 0, sizeof(buf));
	return ret == 0x5555 ? PASS : FAIL;
}
//                                              //
//////////////////////////////////////////////////

int main(void)
{
	return dobug();
}
