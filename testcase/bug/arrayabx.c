/*
 *
 * Bug testcase for GCC6809
 *
 *  PASS: -O0 -O1
 *  FAIL: -O2 -O3 -Os
 *
 */

#include <testcase.h>

uint8 ptr = 2;
uint8 array[4] = { 1, 2, 3, 4 };

NOINLINE uint8 getindex(void)
{
	return 2;
}

//////////////////////////////////////////////////
//                                              //
// function under test                          //
//                                              //
NOINLINE void dobug(void)
{
	uint8 index = getindex();
	array[index] = array[--ptr];
}
//                                              //
//////////////////////////////////////////////////

int main(void)
{
	dobug();

#ifdef VERBOSE
	{
		int i;
		for (i=0; i<4; i++)
			print("%2", array[i]);
		print(" ");
	}
#endif

	return array[2] != 2 ? FAIL : PASS;
}
