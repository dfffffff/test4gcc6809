/*
 *
 * Bug testcase for GCC6809
 *
 *  https://gcc.gnu.org/viewcvs/gcc?view=revision&revision=205117
 *
 *  PASS: -O0 -O1 -O3
 *  FAIL: -O2 -Os
 *
 */

#include <testcase.h>

NOINLINE int strlen(char *str)
{
	(void)str;
	return 4;
}

//////////////////////////////////////////////////
//                                              //
// function under test                          //
//                                              //
NOINLINE void dobug(char *out, unsigned max, ...)
{
	unsigned len;
	char *src;
    va_list ap;
    va_start(ap, max);
    for (; (src = va_arg(ap, char *)); )
    {
        len = strlen(src);
        if (len > max - 1)
            len = max - 1;
        max -= len;
        while (len--)
            *out++ = *src++;
        if (max <= 1)
            break;
    }
    va_end(ap);
    *out = '\0';
}
//                                              //
//////////////////////////////////////////////////

NOINLINE void check(void)
{
	char msg[64];
	dobug(msg, sizeof(msg), "1234", NULL);
#ifdef VERBOSE
	print("%s ", msg);
#endif
	exit(msg[4] ? FAIL : PASS);
}

NOINLINE void setstack(void)
{
	char *stack;
	stack = (char*)__builtin_alloca(256);
	memset(stack, 0x39, 256);
	stack[256-52] = 0;
}

int main(void)
{
	setstack();
	check();
	return FAIL;
}
