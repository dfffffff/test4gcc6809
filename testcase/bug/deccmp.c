/*
 *
 * Bug testcase for GCC6809
 *
 *  https://gitlab.com/dfffffff/gcc6809/issues/1
 *
 *  PASS: -O0 -O1
 *  FAIL: -O2 -O3 -Os
 *
 */

#include <testcase.h>

int8 a;		

//////////////////////////////////////////////////
//                                              //
// function under test                          //
//                                              //
NOINLINE void dobug(void)
{
	if (--a)
	{
		if (a < 0)
		{
			a = 0;
		}
	}
}
//                                              //
//////////////////////////////////////////////////

int main(void)
{
	a = -1;
	asm volatile("clrb");
	asm volatile("andcc #224");
	asm volatile("orcc #1");
	dobug();
#ifdef VERBOSE
	print("%2 ", a);
#endif
	return a != 0 ? FAIL : PASS;
}
