#!/bin/sh
cd "$(dirname "$(readlink -f "$0")")"
export PLATFORM=dfboard
export UTILITY='boardio TRY Z L -VX $1 O0'
export GCC=m6809-unknown-none-gcc
export EXEEXT=.s19
exec script/runtest.sh "$@"
