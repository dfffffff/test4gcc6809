/*
 * Copyright (C) 2016 David Flamand, All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * Minimal runtime
 *
 */

#include <stdarg.h>

#define NOINLINE __attribute__((noinline))
#define NORETURN __attribute__((noreturn))
#define UNUSED __attribute__((unused))

/* logicalbithi.c FAIL with typedef */
#if 1
#define int8 signed char
#define uint8 unsigned char
#define int16 signed short
#define uint16 unsigned short
#else
typedef signed char int8;
typedef unsigned char uint8;
typedef signed short int16;
typedef unsigned short uint16;
#endif

#define NULL 0
#define PASS 0
#define FAIL 1

/* provided by platform runtime */
extern void write(void *buf, int len);

/* provided by crt0.o */
//extern NORETURN void _exit(int);

/* may be referenced by crt0.o */
NOINLINE void atexit(void* d)
{
	(void)d;
}

/* may be referenced by crt0.o */
NOINLINE void *memset(void *ptr, int value, unsigned int count)
{
	char *cptr = (char*)ptr;
	while (count != 0) {
		*cptr++ = value;
		count--;
	}
	return ptr;
}

NOINLINE void outchar(int8 value)
{
	write(&value, 1);
}

NOINLINE int8 hex(int16 value, int8 shift)
{
	value = (value >> shift) & 0xF;
	return value < 10 ? value + '0' : value - 10 + 'A';
}

/* for debugging purpose */
NOINLINE void print(const char* format, ...)
{
	int d;
	const char *s;
	va_list ap;
	va_start(ap, format);
	while (*format) {
		switch (*format) {
		case '%':
			format++;
			switch (*format++) {
			case '2':
				d = va_arg(ap, int);
				outchar(hex(d, 4));
				outchar(hex(d, 0));
				break;
			case '4':
				d = va_arg(ap, int);
				outchar(hex(d, 12));
				outchar(hex(d, 8));
				outchar(hex(d, 4));
				outchar(hex(d, 0));
				break;
			case 'c':
				d = va_arg(ap, int);
				outchar(d);
				break;
			case 's':
				s = va_arg(ap, const char *);
				while ((d=*s++)) {
					outchar(d);
				}
				break;
			}
			break;
		default:
			outchar(*format++);
			break;
		}
	}
	va_end(ap);
}

NOINLINE NORETURN void exit(int c)
{
	print("%s\n%c", c == PASS ? "PASS" : "FAIL", 0);
	for (;;); /* unsafe for some testcase to return via _exit() */
//	_exit(c);
}

NOINLINE NORETURN void abort(void)
{
	exit(FAIL);
}
