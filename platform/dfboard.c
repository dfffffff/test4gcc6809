/*
 * Platform runtime for the DF's 6809 board rev 1.1
 */

/* Lets the compiler generate the prologue/epilogue for us,
   no need to fiddle with the calling convention. */
void write(void *buf, int len)
{
	asm volatile (
"     ldx  %[len]  \n"
"     beq  ret     \n"
"     ldy  %[buf]  \n"
"loo: ldb  ,y+     \n"
"     jsr  [0xFEE2] \n"
"     leax -1,x    \n"
"     bne  loo     \n"
"ret:               "
		:
		: [buf] "m" (buf), [len] "m" (len)
		: "cc", "y"
	);
}
